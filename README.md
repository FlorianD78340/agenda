# Agenda

Dégager des cas d'utilisation d'une application d'agenda.

# Cahier des charges: 
Nous désirons implanter la gestion d’un agenda:  
- Un agenda contient un ensemble de personnes  
- Un agenda possède un propriétaire  
- Chaque personne est identifiée par son nom et par un ensemble de coordonnées  
- Une coordonnée peut être postale, téléphonique ou électonique (email ou page web)  

Cette application sera implémentée en 2-tiers. Il est attendu à ce qu'il y ait la structure "classique"   
avec un front end qui interroge un back end. Le back-end devra respecter les bonnes pratiques d'une API Rest.  
Nulle technologie n'a été spécifiée.

# Diagramme de cas d'utilisation

![Use case diagram](./useCaseAgenda.PNG)

# Route HTTP des cas d'utilisation de l'agenda

**Gestion d'un agenda:**  
- Lister des contacts : ` GET /agenda/1/contact `  
- Détailler un contact : ` GET /agenda/1/contact/100 `
- Trier les contacts : ` GET /agenda/1/contact `

- Ajouter un contact : ` POST /agenda/1/contact/nom=...&address=...&numero=... `  

- Retirer un contact : ` DELETE /agenda/1/contact/100/ `  

- Modifier un contact : ` PATCH /agenda/1/contact/100/adress=newaddress&numero=newnum `  

# Accéder à l'api  

Installer FastAPI: `pip install fastapi`  
Installer Uvicorn: `pip install uvicorn`

Lancer l'application: `uvicorn app:app --reload`

http://127.0.0.1:8000/agenda  
http://127.0.0.1:8000/docs -> interface swagger  

Ajout de "ami12" dans l'agenda: 'http://127.0.0.1:8000/agenda?contact=ami12'
