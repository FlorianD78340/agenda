from fastapi import FastAPI

app = FastAPI()


agenda = {
    'contact': [
        'Papa',
        'Maman',
        'Frere',
        'Soeur',
        'Ami',
        'Amie',
        'Collegue']
}


@app.get("/agenda")
async def get_agenda():
    return {'agenda': agenda}, 200


@app.post("/agenda")
async def post_agenda(contact: str):
    if contact in agenda['contact']:
        return {'agenda': agenda, 'message': "Le contact existe déjà"}

    else:
        agenda['contact'].append(contact)
        return {'agenda': agenda, 'message': "Le contact a été ajouté"}


@app.delete("/agenda")
async def delete_lieu(contact: str):
    if contact in agenda['contact']:
        agenda['contact'].remove(contact)
        return {'contact': agenda, 'message': 'le contact est supprimé'}

    else:
        return {'contact': agenda, 'message': "le contact n'existe pas"}
